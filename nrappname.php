<?php
/*
Plugin Name: New Relic App Name
Description: Allows for setting the New Relic App Name of the WordPress App.
Version: 01.0
Author: Paul Tela
Author URI: http://www.paultela.com/
License: MIT
*/

class NewRelicAppName
{
	public static function settingsSubMenuPage()
	{
		add_options_page("New Relic App Name", "New Relic App Name", "manage_options", "new-relic-app-name", ['NewRelicAppName', 'settingsPageCreator']);
	}

	public static function registerSettings()
	{
		register_setting( 'nrappname_options', 'nrappname_options', ['NewRelicAppName', 'validateOptions']);
		add_settings_section('nrappname_main', 'New Relic App Name', ['NewRelicAppName', 'sectionText'], 'nrappname');
		add_settings_field('nrappname_app_name', 'New Relic App Name', ['NewRelicAppName', 'inputField'], 'nrappname', 'nrappname_main');
	}

	public static function settingsPageCreator()
	{
		if (!current_user_can('manage_options')) {
			wp_die(__("You do not have sufficient permissions to access this page."));
		}

		echo "<div class='nr-appname'>
			<h3>Set NewRelic App Name</h3>";

		if (!extension_loaded('newrelic')) {
			echo "<p><strong>The New relic Extension is not active!</strong></p>";
		}

		echo "<form method='post' action='options.php'>";

			settings_fields('nrappname_options');
			do_settings_sections('nrappname');
			submit_button();

			echo "</form></div>";
	}

	public static function validateOptions($input)
	{
		$newinput = [];
		$newinput['app_name'] = trim($input['app_name']);

		return $newinput;
	}

	public static function sectionText()
	{
		echo "<p>Set the app_name you would like to send to New Relic</p>";
	}

	public static function inputField()
	{
		$options = get_option('nrappname_options');
		echo "<input id='nrappname_app_name' name='nrappname_options[app_name] type='text' value='{$options['app_name']}' />";
	}

	public static function addAppName()
	{
		if (extension_loaded('newrelic')) {
			$app_name_options = get_option('nrappname_options');
			if (!empty($app_name_options['app_name'])) {
				newrelic_set_appname($app_name_options['app_name']);
			}
		}
	}
}

add_action('plugins_loaded', ['NewRelicAppName', 'addAppName']);
add_action('admin_menu', ['NewRelicAppName', 'settingsSubMenuPage']);
add_action('admin_init', ['NewRelicAppName', 'registerSettings']);
